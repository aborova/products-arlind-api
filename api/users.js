const express = require('express');
const knex = require('../db/knex');
const router = express.Router();
const User = require("../models/user");


router.get('/', (req,res) => {
    User.fetchAll().then(users=> {
        res.json(users);
    })
})

router.get('/:id', (req,res, next) => {
    User.where('id', req.params.id).fetch().then(user=> {
        res.json(user);
    }).catch((err)=>{
        res.status(404);
        next();
      });
})

router.get('/order/:id', (req,res, next) => {
    knex('orders').where('user_id', req.params.id).first().count('*').then(user=> {
        if(user && user.count != 0){
        res.json(user);
        } else{
            res.status(404);
            next();
        }
    })
})

module.exports = router;
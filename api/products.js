const express = require('express');

const router = express.Router();

const knex = require('../db/knex');
const Product = require("../models/product");

router.get('/', (req,res) => {
    Product.fetchAll().then(products=> {
        res.json(products);
    })
})

router.get('/average', (req,res) => {
    knex('products').avg('price').then(products=> {
        res.json(products);
    })
})


module.exports = router;
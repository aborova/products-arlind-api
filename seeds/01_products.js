
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('products').del()
    .then(function () {
      // Inserts seed entries
      return knex('products').insert([
        {title: 'ananas', price: 300},
        {title: 'melon', price: 125}
      ]);
    });
};

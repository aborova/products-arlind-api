exports.up = function(knex) {  
    return Promise.all([
      knex.schema.createTable('products', function(table){
        table.increments('id').primary();
        table.string('title');
        table.integer('price');
      }),
      knex.schema.createTable('orders', function(table){
        table.increments('id').primary();
        table.integer('product_id').references('products.id');
        table.integer('user_id').references('users.id');
      }),
      knex.schema.createTable('users', function(table){
        table.increments('id').primary();
        table.string('first_name');
        table.string('last_name');
        table.string('age');
      })
    ]);
  };
  
  exports.down = function(knex, Promise) {  
    return Promise.all([
      knex.schema.dropTable('products'),
      knex.schema.dropTable('orders'),
      knex.schema.dropTable('users')
    ]);
  };
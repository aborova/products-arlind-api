const bookshelf = require('../bookshelf')

const Product = bookshelf.model('Product', {
    tableName: 'products',
    users() {
      return this.belongsToMany('User')
    }
  })
  
  module.exports = Product
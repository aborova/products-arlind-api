const bookshelf = require('../bookshelf')

  const User = bookshelf.model('User', {
    tableName: 'users',
    products() {
      return this.belongsToMany('Product')
    }
  })

  module.exports = User